﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;

namespace SignalRtest2
{
    public class ShapeHub : Hub
    {
        public void Send(string name, string message)
        {
            Clients.Others.broadcastMessage(name, message);
        }
    }
}