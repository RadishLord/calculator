﻿$(document).ready(function ()
{
    var first; //первая переменная
    var temp = null; //текущая переменная
    var result; //результат
    var phase = 1; //фаза
    var phase_op;
    var symbol; //знак действия
    var undead; //бессмертный темп
    var block; //чтобы знак не работал при повторном нажатии
    var comma = null; //флаг запятой

    //////////////////////////////////
    $("#comma").click(function () {
        comma = 1;
        $("#text").text(temp+".")
    })
    //////////////////////////////////
    $(".Main").draggable();
    //////////////////////////////////
    temp = 0;
    $("#text").text(temp);
    //////////////////////////////////
    $("#check").click(function () {
        alert("first = " + first + " temp =" + temp + " result =" + result + " фаза =" + phase + " undead " + undead + " block =" + block + " comma =" + comma);
    });
    //////////////////////////////////
    $("#1").click(function () {
        if (result == temp) {                       //--Если на экране результат предыдущего действия
            temp = null;                            //--обнцуляет переменные
            phase = 1;
            result = 0;
        }
        if (comma !== null) {                       //--Выполняется если юзер вводит дробное число     
            var st = 1;
            for (i = 1; i <= comma; i++)
                st = st / 10;
            temp = temp + st;
            //temp = temp.toFixed(comma);
            if (phase == 1)                         //--Вывод при первой фазе
            { $("#text").text(temp) }
            else {                                  //--Вывод при второй фазе
                $("#text1").text(first + symbol)
                $("#text").text(temp)
            }
            comma = comma + 1;                      //--Увеличивает количество знаков после запятой
        }
        else {
          
            if (temp == null) {
                temp = 1;
            }
            else {
                temp = temp * 10 + 1;
            }
            if (phase == 1)
            { $("#text").text(temp) }               //--Вывод при первой фазе
            else {                                  //--Вывод при второй фазе
                $("#text1").text(first + symbol)
                $("#text").text(temp)
            }
        }
        block = 0;                                  //--Снимает блокировку с операций
    });
    //////////////////////////////////
    $("#2").click(function () {
        if (result == temp) {                       //--Если на экране результат предыдущего действия
            temp = null;                            //--обнцуляет переменные
            phase = 1;
            result = 0;
            //first = 0;
        }
        if (comma !== null) {                       //--Выполняется если юзер вводит дробное число     
            var st = 2;
            for (i = 1; i <= comma; i++)
                st = st / 10;
            temp = temp + st;
            if (phase == 1)                         //--Вывод при первой фазе
            { $("#text").text(temp) }
            else {                                  //--Вывод при второй фазе
                $("#text1").text(first + symbol)
                $("#text").text(temp)
            }
            comma = comma + 1;                      //--Увеличивает количество знаков после запятой
        }
        else {
          
            if (temp == null) {
                temp = 2;
            }
            else {
                temp = temp * 10 + 2;
            }
            if (phase == 1)
            { $("#text").text(temp) }               //--Вывод при первой фазе
            else {                                  //--Вывод при второй фазе
                $("#text1").text(first + symbol)
                $("#text").text(temp)
            }
        }
        block = 0;                                  //--Снимает блокировку с операций
    });
    //////////////////////////////////
    $("#3").click(function () {
        if (result == temp) {                       //--Если на экране результат предыдущего действия
            temp = null;                            //--обнцуляет переменные
            phase = 1;
            result = 0;
            //first = 0;
        }
        if (comma !== null) {                       //--Выполняется если юзер вводит дробное число     
            var st = 3;
            for (i = 1; i <= comma; i++)
                st = st / 10;
            temp = temp + st;
            if (phase == 1)                         //--Вывод при первой фазе
            { $("#text").text(temp) }
            else {                                  //--Вывод при второй фазе
                $("#text1").text(first + symbol)
                $("#text").text(temp)
            }
            comma = comma + 1;                      //--Увеличивает количество знаков после запятой
        }
        else {

            if (temp == null) {
                temp = 3;
            }
            else {
                temp = temp * 10 + 3;
            }
            if (phase == 1)
            { $("#text").text(temp) }               //--Вывод при первой фазе
            else {                                  //--Вывод при второй фазе
                $("#text1").text(first + symbol)
                $("#text").text(temp)
            }
        }
        block = 0;                                  //--Снимает блокировку с операций
    });
    //////////////////////////////////
    $("#4").click(function () {
        if (result == temp) {                       //--Если на экране результат предыдущего действия
            temp = null;                            //--обнцуляет переменные
            phase = 1;
            result = 0;
            //first = 0;
        }
        if (comma !== null) {                       //--Выполняется если юзер вводит дробное число     
            var st = 4;
            for (i = 1; i <= comma; i++)
                st = st / 10;
            temp = temp + st;
            if (phase == 1)                         //--Вывод при первой фазе
            { $("#text").text(temp) }
            else {                                  //--Вывод при второй фазе
                $("#text1").text(first + symbol)
                $("#text").text(temp)
            }
            comma = comma + 1;                      //--Увеличивает количество знаков после запятой
        }
        else {

            if (temp == null) {
                temp = 4;
            }
            else {
                temp = temp * 10 + 4;
            }
            if (phase == 1)
            { $("#text").text(temp) }               //--Вывод при первой фазе
            else {                                  //--Вывод при второй фазе
                $("#text1").text(first + symbol)
                $("#text").text(temp)
            }
        }
        block = 0;                                  //--Снимает блокировку с операций
    });
    //////////////////////////////////
    $("#5").click(function () {
        if (result == temp) {                       //--Если на экране результат предыдущего действия
            temp = null;                            //--обнцуляет переменные
            phase = 1;
            result = 0;
            //first = 0;
        }
        if (comma !== null) {                       //--Выполняется если юзер вводит дробное число     
            var st = 5;
            for (i = 1; i <= comma; i++)
                st = st / 10;
            temp = temp + st;
            if (phase == 1)                         //--Вывод при первой фазе
            { $("#text").text(temp) }
            else {                                  //--Вывод при второй фазе
                $("#text1").text(first + symbol)
                $("#text").text(temp)
            }
            comma = comma + 1;                      //--Увеличивает количество знаков после запятой
        }
        else {

            if (temp == null) {
                temp = 5;
            }
            else {
                temp = temp * 10 + 5;
            }
            if (phase == 1)
            { $("#text").text(temp) }               //--Вывод при первой фазе
            else {                                  //--Вывод при второй фазе
                $("#text1").text(first + symbol)
                $("#text").text(temp)
            }
        }
        block = 0;                                  //--Снимает блокировку с операций
    });
    //////////////////////////////////
    $("#6").click(function () {
        if (result == temp) {                       //--Если на экране результат предыдущего действия
            temp = null;                            //--обнцуляет переменные
            phase = 1;
            result = 0;
            //first = 0;
        }
        if (comma !== null) {                       //--Выполняется если юзер вводит дробное число     
            var st = 6;
            for (i = 1; i <= comma; i++)
                st = st / 10;
            temp = temp + st;
            if (phase == 1)                         //--Вывод при первой фазе
            { $("#text").text(temp) }
            else {                                  //--Вывод при второй фазе
                $("#text1").text(first + symbol)
                $("#text").text(temp)
            }
            comma = comma + 1;                      //--Увеличивает количество знаков после запятой
        }
        else {

            if (temp == null) {
                temp = 6;
            }
            else {
                temp = temp * 10 + 6;
            }
            if (phase == 1)
            { $("#text").text(temp) }               //--Вывод при первой фазе
            else {                                  //--Вывод при второй фазе
                $("#text1").text(first + symbol)
                $("#text").text(temp)
            }
        }
        block = 0;                                  //--Снимает блокировку с операций
    });
    //////////////////////////////////
    $("#7").click(function () {
        if (result == temp) {                       //--Если на экране результат предыдущего действия
            temp = null;                            //--обнцуляет переменные
            phase = 1;
            result = 0;
            //first = 0;
        }
        if (comma !== null) {                       //--Выполняется если юзер вводит дробное число     
            var st = 7;
            for (i = 1; i <= comma; i++)
                st = st / 10;
            temp = temp + st;
            if (phase == 1)                         //--Вывод при первой фазе
            { $("#text").text(temp) }
            else {                                  //--Вывод при второй фазе
                $("#text1").text(first + symbol)
                $("#text").text(temp)
            }
            comma = comma + 1;                      //--Увеличивает количество знаков после запятой
        }
        else {

            if (temp == null) {
                temp = 7;
            }
            else {
                temp = temp * 10 + 7;
            }
            if (phase == 1)
            { $("#text").text(temp) }               //--Вывод при первой фазе
            else {                                  //--Вывод при второй фазе
                $("#text1").text(first + symbol)
                $("#text").text(temp)
            }
        }
        block = 0;                                  //--Снимает блокировку с операций
    });
    //////////////////////////////////
    $("#8").click(function () {
        if (result == temp) {                       //--Если на экране результат предыдущего действия
            temp = null;                            //--обнцуляет переменные
            phase = 1;
            result = 0;
            //first = 0;
        }
        if (comma !== null) {                       //--Выполняется если юзер вводит дробное число     
            var st = 8;
            for (i = 1; i <= comma; i++)
                st = st / 10;
            temp = temp + st;
            if (phase == 1)                         //--Вывод при первой фазе
            { $("#text").text(temp) }
            else {                                  //--Вывод при второй фазе
                $("#text1").text(first + symbol)
                $("#text").text(temp)
            }
            comma = comma + 1;                      //--Увеличивает количество знаков после запятой
        }
        else {

            if (temp == null) {
                temp = 8;
            }
            else {
                temp = temp * 10 + 8;
            }
            if (phase == 1)
            { $("#text").text(temp) }               //--Вывод при первой фазе
            else {                                  //--Вывод при второй фазе
                $("#text1").text(first + symbol)
                $("#text").text(temp)
            }
        }
        block = 0;                                  //--Снимает блокировку с операций
    });
    //////////////////////////////////
    $("#9").click(function () {
        if (result == temp) {                       //--Если на экране результат предыдущего действия
            temp = null;                            //--обнцуляет переменные
            phase = 1;
            result = 0;
            //first = 0;
        }
        if (comma !== null) {                       //--Выполняется если юзер вводит дробное число     
            var st = 2;
            for (i = 1; i <= comma; i++)
                st = st / 10;
            temp = temp + st;
            if (phase == 1)                         //--Вывод при первой фазе
            { $("#text").text(temp) }
            else {                                  //--Вывод при второй фазе
                $("#text1").text(first + symbol)
                $("#text").text(temp)
            }
            comma = comma + 1;                      //--Увеличивает количество знаков после запятой
        }
        else {

            if (temp == null) {
                temp = 9;
            }
            else {
                temp = temp * 10 + 9;
            }
            if (phase == 1)
            { $("#text").text(temp) }               //--Вывод при первой фазе
            else {                                  //--Вывод при второй фазе
                $("#text1").text(first + symbol)
                $("#text").text(temp)
            }
        }
        block = 0;                                  //--Снимает блокировку с операций
    });
    //////////////////////////////////
    $("#0").click(function () {
        if (result == temp) {                       //--Если на экране результат предыдущего действия
            temp = null;                            //--обнцуляет переменные
            phase = 1;
            result = 0;
            //first = 0;
        }
        if (comma !== null) {                       //--Выполняется если юзер вводит дробное число     
            temp = temp / 10;
            if (phase == 1)                         //--Вывод при первой фазе
            { $("#text").text(temp) }
            else {                                  //--Вывод при второй фазе
                $("#text1").text(first + symbol)
                $("#text").text(temp)
            }
            comma = comma + 1;                      //--Увеличивает количество знаков после запятой
        }
        else {

            if (temp == null) {
                temp = 0;
            }
            else {
                temp = temp * 10;
            }
            if (phase == 1)
            { $("#text").text(temp) }               //--Вывод при первой фазе
            else {                                  //--Вывод при второй фазе
                $("#text1").text(first + symbol)
                $("#text").text(temp)
            }
        }
        block = 0;                                  //--Снимает блокировку с операций
    });
    //////////////////////////////////
    $("#quad").click(function () {
        if ((temp == null) && (first !== null))
            temp = first;
        $("#text1").text("quad(" + temp + ")")
        temp = temp * temp;
        $("#text").text(temp)
    });
    //////////////////////////////////
    $("#sqrt").click(function () {
        $("#text1").text("sqrt(" + temp + ") ")
        $("#text").text(Math.sqrt(temp))
        temp = Math.sqrt(temp);
        phase = 3;
    });
    //////////////////////////////////
    $("#proc").click(function () {
        
        qq = temp;
        temp = first * qq / 100;
        $("#text").text(temp)
        phase = 3;
        
    });
    //////////////////////////////////
    $("#del").click(function () {
        if (phase !== 2)
        {
            if (comma !== null) {                  //--если число дробное 
                if (comma == 1) {                  //--Когда дробная часть пуста
                    comma = null;
                    $("#text").text(temp)
                }
                else {
                    var st = 100;
                    for (i = 0; i < comma; i++)
                        st = st / 10;
                    temp = (temp - (temp % st));
                    comma = comma - 1
                    temp = temp.toFixed(comma - 1)
                    if (comma == 1)
                        $("#text").text(temp + ".")
                    else
                        $("#text").text(temp)
                }
            }   
            else {                                //--Если число целое
                temp = (temp - (temp % 10)) / 10;
                $("#text").text(temp)
            }
        }
    });
    //////////////////////////////////
    $("#c").click(function () {
        temp = 0;
        first = null;
        result = null;
        phase = 1;
        phase_op = null;
        undead = 0;
        comma = null;
        $("#text").text("0")
        $("#text1").text(null)
    });
    //////////////////////////////////
    $("#ce").click(function () {
        temp = 0;
        if (phase == 2) {
            $("#text").text(temp)
        }
        else {
            result = null;
            first = null;
            comma = null;
            phase = 1;
            $("#text").text("0")
        }
        
    });
    //////////////////////////////////
    $("#znak").click(function () {
        temp = temp * (-1);
        $("#text").text(temp)
    });
    //////////////////////////////////
    $("#plus").click(function () {
        if (block !== 1) {
            if (phase == 1) {           //--плюс нажимается певрый раз
                first = temp;
                temp = null;
                symbol = "+";
                $("#text1").text(first + "+")
                $("#text").text(first)
                phase_op = 1;   
            }
            else {                      //--повторные нажатия
                var ff;
                ff = temp
                temp = first + ff;
                if (phase_op == null) {
                    alert("op = null")
                    $("#text1").text(first + "+")
                    $("#text").text(first)
                }
                else if (phase_op == 1) {
                    $("#text1").text(temp + "+")
                    $("#text").text(null)
                }
                first = temp;
                temp = null;
            }
        }
        comma = null;
        block = 1;
        phase = 2;
    });
    //////////////////////////////////
    $("#minus").click(function () {
        if (block !== 1) {
            if (phase == 1) {           //--плюс нажимается певрый раз
                first = temp;
                temp = null;
                symbol = "-";
                $("#text1").text(first + "-")
                $("#text").text(first)
                phase_op = 1;
            }
            else {                      //--повторные нажатия
                var ff;
                ff = temp
                temp = first - ff;
                if (phase_op == null) {
                    alert("op = null")
                    $("#text1").text(first + "-")
                    $("#text").text(first)
                }
                else if (phase_op == 1) {
                    $("#text1").text(temp + "-")
                    $("#text").text(null)
                }
                first = temp;
                temp = null;
            }
        }
        comma = null;
        block = 1;
        phase = 2;

    });
    //////////////////////////////////
    $("#multiplication").click(function () {
        if (block !== 1) {
            if (phase == 1) {           //--плюс нажимается певрый раз
                first = temp;
                temp = null;
                symbol = "*";
                $("#text1").text(first + "*")
                $("#text").text(first)
                phase_op = 1;
            }
            else {                      //--повторные нажатия
                var ff;
                ff = temp
                temp = first * ff;
                if (phase_op == null) {
                    alert("op = null")
                    $("#text1").text(first + "*")
                    $("#text").text(first)
                }
                else if (phase_op == 1) {
                    $("#text1").text(temp + "*")
                    $("#text").text(null)
                }
                first = temp;
                temp = null;
            }
        }
        comma = null;
        block = 1;
        phase = 2;

    });
    //////////////////////////////////
    $("#division").click(function () {
        if (block !== 1) {
            if (phase == 1) {           //--плюс нажимается певрый раз
                first = temp;
                temp = null;
                symbol = "/";
                $("#text1").text(first + "/")
                $("#text").text(first)
                phase_op = 1;
            }
            else {                      //--повторные нажатия
                var ff;
                ff = temp
                temp = first / ff;
                if (phase_op == null) {
                    alert("op = null")
                    $("#text1").text(first + "/")
                    $("#text").text(first)
                }
                else if (phase_op == 1) {
                    $("#text1").text(temp + "/")
                    $("#text").text(null)
                }
                first = temp;
                temp = null;
            }
        }
        comma = null;
        block = 1;
        phase = 2;
    });
    //////////////////////////////////
    $("#reci").click(function () {
        $("#text1").text("reciproc (" + temp + ") ")
        $("#text").text(1 / temp)
    });
    //////////////////////////////////
    $("#eq").click(function () {
        if (temp == null)
            temp = first;    //на случай если юзер не задал второе число
        if (phase_op == 2) {
            if (symbol == "+") {

                result = temp + undead;
            }
            ////////////////////
            if (symbol == "-") {

                result = temp - undead;
            }
            ////////////////////
            if (symbol == "*") {

                result = temp * undead;
            }
            if (symbol == "/") {

                result = temp / undead;
            }
        }
        else {
            undead = temp;
            if (symbol == "+") {

                result = first + temp;
            }
            ////////////////////
            if (symbol == "-") {

                result = first - temp;
            }
            ////////////////////
            if (symbol == "*") {

                result = first * temp;
            }
            if (symbol == "/") {

                result = first / temp;
            }
        }
        $("#text1").text(null)
        $("#text").text(result)
        temp = result;
        phase = 1;
        block = 0;
        phase_op = 2;
    });
    //////////////////////////////////

})