﻿$(document).ready(function ()
{
    var temp = null;    //--Переменная в которую организуется ввод
    var first = null;   //--Первое число   
    var second = null;  //--Второе число
    var symbol;         //--Знак операции
    var old_symbol;     //--Знак предыдущей операции
    var phase = 1;      //--фаза. 1 - вводится первое число, 2 - нажимается знак действия
    var res_flag;       //--1 если на экране результат предыдущего действия
    var block = 1;		//--Блокирует кнопку равно
	var un_flag = 0;    //--Флаг унарных операциий
	var coma_flag  = 0; //--флаг точки
	var kolv = null;    //--количество чисел после запятой
	var data;			//--снимет символс кнопки
	
    $("#check").click(function () {
        alert("temp =" + temp + " first =" + first + " second =" + second + " phase =" + phase + block);
		alert("coma_flag:" + coma_flag + "  lokv:" + kolv)
    });
    //////////--Подвижность формы--///
    $(".Main").draggable({ cursor: "move" });
    /////--Числовые кнопки////////////
    $(".number").click(function () {
	
		data = $(this).text();
		
		if ((coma_flag == 1) && (data == ".") || ((temp == 0) && (data == "0")&&(coma_flag == 0))) {
            //обработка ситуаций когда цифры не должны вводится
        }
        else {
	
		
			if ((data == ".")&&(temp == null))
				temp = 0;
				
			if (coma_flag == 1){
				kolv = kolv + 1;
			}
			
		
			if (data == "."){
				coma_flag = 1;
			}
		
			if (un_flag == 1)
			{			//--Если на экране результат унарной операции
			un_flag = 0;			//--обнуляет temp и чистит экран
			temp = null;
			$("#area1").text(null);
			}
			
	
			if (res_flag == 1) {        //--Если на экране предыдущий результат 
            temp = null;            //--обнуляет темп
            res_flag = 0;
			
			}
			if (temp == null) {           //--Ввод первой цифры
            temp = data;
			}
			else {                        //--Ввод последующих цифр
				if ((temp == "0") && (data != "." && temp == null))
                temp = data;
            else
                temp = temp + data;
			}
			$("input").val(temp);

			if (phase == 1) {           //--Присваевание в зависимости от фазы
				first = temp;
				block = 1;
			}
			else if (phase == 2)
				second = temp;
		}//end else
			
		
    });
    //////////////--СЕ--//////////////
    $("#ce").click(function () {
        temp = null;
		kolv = 0;
        $("input").val("0");
    });
    //////////////--С--///////////////
    $("#c").click(function () {
        temp = null;
        first = null;
        second = null;
        phase = 1; 
		kolv = 0;
		coma_flag = 0;
        $("#area1").text(null);
        $("input").val("0");
    });
    ////--Удалить символ--////////////
    $("#del").click(function () {
		if (coma_flag == 1)
            kolv = kolv - 1;
		temp = temp.slice(0, -1);
		$("input").val(temp);
    });
    ////--Унарные операции--//////////
    $(".uno").click(function () {
        var un_simbol
		un_flag = 1;
		block = 1;
        un_simbol = $(this).text();
        if (un_simbol == "Х²") {
            $("#area1").text("sqr (" + temp + ")");
            temp = temp * temp;
            $("input").val(temp);
        }
        if (un_simbol == "√") {
            $("#area1").text("sqrt (" + temp + ")");
            temp = Math.sqrt(temp);
            $("input").val(temp);
        } 
        if (un_simbol == "1/x") {
            $("#area1").text("reciproc (" + temp + ")");
            temp = 1 / temp;
            $("input").val(temp);
        }
        first = temp;
    })
    ////--Бинарные операции--/////////
    $(".bin").click(function () {
	
	if (temp == null){
		temp = 0;
		first = 0;
		phase = 2;
	}
		
	
	coma_flag = 0;
        if (phase == 1) {
            symbol = $(this).text();
            $("#area1").text(temp + symbol);
            $("input").val(temp);
            phase = 2;
            temp = null;
            old_symbol = symbol;
            block = null;
        } else {                            //--при повторном нажатии на знак операции
            if (second == null)
                second = first;

            $("input").val(first);
            symbol = $(this).text();        //--Узнает новый символ

            if (old_symbol == "+")
            { first = (first * 1) + (second * 1); }

            if (old_symbol == "-")
            { first = first - second; }

            if (old_symbol == "×")
            { first = (first * 1) * (second * 1);
			  first = first.toFixed(kolv)		}

            if (old_symbol == "÷")
            {  first = (first * 1) / (second * 1); }

            old_symbol = symbol;            //--Запоминает текущий символ

            $("#area1").append(second + old_symbol);
            $("input").val(first);
            temp = null;
            block = null;
        }
        
    })
    /////////--Смена знака--//////////
    $("#sign").click(function () {
        temp = temp * (-1);
		if (phase == 1) {           //--Присваевание в зависимости от фазы
				first = temp;
			}
			else if (phase == 2)
				second = temp;
        $("input").val(temp);
    });
    /////////--Процент--//////////////
    $("#proc").click(function () {

        qq = temp;
        temp = first * qq / 100;
        $("input").val(temp);

    });
    //////////--Ранво--///////////////
    $("#eq").click(function () {
        if (block == null) {
            var result;
            if (second == null)
                second = first;
            if (first == null)
                first = temp;
            if (symbol == "+")
            { 
				result = (first * 1) + (second * 1);
				result = result.toFixed (kolv);		
			}

            if (symbol == "-")
            { result = first - second; }

            if (symbol == "×")
            { 
				result = (first * 1) * (second * 1); 
				result = result.toFixed(kolv)
			}

            if (symbol == "÷")
            { result = (first * 1) / (second * 1); }

            $("#area1").text(null);
            $("input").val(result);
            temp = result;
            first = null;
            phase = 1;
            res_flag = 1;
        }
        
    });

})